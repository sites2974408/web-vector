<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/article', [App\Http\Controllers\Api\V1\ArticleController::class, 'index'])->name('articles');
Route::get('/article/{id}', [App\Http\Controllers\Api\V1\ArticleController::class, 'detail'])->name('articles.detail');
Route::get('/parse', [App\Http\Controllers\Api\V1\ArticleController::class, 'parse'])->name('parse');

Route::get('/portfolio', [App\Http\Controllers\Api\V1\PortfolioController::class, 'index'])->name('parse');

Route::post('/send-mail', function (Request $request) {

    $details = [
        'title' => 'Mail from WebVector.ru',
        'body' => 'Письмо с сайта web-vector.ru',
    ];

    $details = array_merge($details, $request->all());

    \Mail::to('sale@web-vector.ru')->send(new \App\Mail\Contact($details));

    return redirect()->back()
        ->with(['success' => 'Спасибо за обращение, мы свяжемся с Вами!']);
});

