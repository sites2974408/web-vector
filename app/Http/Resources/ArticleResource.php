<?php

namespace App\Http\Resources;

use App\Models\Article;
use Illuminate\Http\Resources\Json\JsonResource;

class ArticleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {

        $arReturn = parent::toArray($request) +
            ['link' => '/' . $this->article_id . '/' . $this->code]+
            ['categories' => Article::find($this->id)->categories];
        return $arReturn;
    }
}
