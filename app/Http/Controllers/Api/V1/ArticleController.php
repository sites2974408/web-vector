<?php

namespace App\Http\Controllers\Api\V1;

use App\Http\Controllers\Controller;
use App\Http\Resources\ArticleCollection;
use App\Http\Resources\ArticleResource;
use App\Library\Services\YandexTranslate;
use App\Models\Article;
use App\Models\Category;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Spatie\Crawler\Crawler;
use App\Observers\ArticleCrawlerObserver;
use GuzzleHttp\RequestOptions;
use GuzzleHttp\Client;


class ArticleController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \App\Http\Resources\ArticleCollection
     */
    public function index(Request $request) {

        return new ArticleCollection(Cache::remember('articles_' . $request->get('page'), 60*60*24, function (){
            return Article::orderByDesc('article_id')->paginate(18);
        }));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @ToDo Доработать получение страниц с ?page=n
     * @return \App\Http\Resources\ArticleCollection
     */
    public function detail($value) {
        $this->iId = $value;
        return new ArticleResource(Cache::remember('article_' . $this->iId, 60*60*24, function (){
            return Article::where('article_id', $this->iId)->first();
        }));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function show(Article $article)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function edit(Article $article)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Article $article)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function destroy(Article $article)
    {
        //
    }

    public function parse($link = 'https://www.infoworld.com/category/web-development/')
    {

        /**
         * @var array $arCrawlers Array of StdObjects with diff of tables
         */
        $arCrawlers = DB::select("
        select * FROM crawlers LEFT JOIN
          (select path as i from articles) as t1
            ON crawlers.path=t1.i where t1.i IS NULL
        HAVING crawlers.path <> ''
");

        foreach ($arCrawlers as $obCrawler) {


            $content = $obCrawler->content;

            $doc = new \DOMDocument();
            @$doc->loadHTML($content);




            $doc->saveXml($doc->documentElement);


            $finder = new \DomXPath($doc);

            /** Replace pre to template for translate */

            /** @var array $arNewElements Элементы*/
            $arNewElements = [];

            $nodes  = $finder->query('//code');
            foreach ($nodes as $k => $obNode) {
                $sKey = "{{#_$k}}";
                $obNode->parentNode->replaceChild($doc->createTextNode($sKey), $obNode);
                $arNewElements[$sKey] = $doc->saveHTML($obNode);
            }

            $nImages  = $finder->query('//img');
            foreach ($nImages as $k => $obImg) {
                if(!empty($obImg->getAttribute('data-original'))) {
                    $obImg->setAttribute('src', $obImg->getAttribute('data-original'));
                }
            }

            $arFind['title'] = strip_tags(
                $doc
                    ->saveHTML(
                        $finder
                            ->query("//*[contains(@itemprop, 'headline')]")
                            ->item(0)
                    )
            );

            $arFind['description'] = strip_tags(
                $doc
                    ->saveHTML(
                        $finder
                            ->query("//*[contains(@itemprop, 'description')]")
                            ->item(0)
                    )
            );
            $arFind['body'] =
                $doc
                    ->saveHTML(
                        $finder
                            ->query("//*[contains(@itemprop, 'articleBody')]")
                            ->item(0)
                    );


            /** Combine arrays of names and translates */
            $arFind = array_combine(array_keys($arFind), YandexTranslate::translate(array_values($arFind)));

            if(empty($arFind['title'])) {continue;}

            /** return <code> to html */
            $arFind['body'] = str_replace(
                array_keys($arNewElements),
                array_values($arNewElements),
                $arFind['body']
            );

             $img = $finder
                ->query("//*[contains(@class, 'feature-img')]")
                ->item(0);

             /** @ToDo доработать для получения записей без картинок */
            if(empty($img)) {continue;}

            $arFind['img'] = $img->getAttribute('src');


            $sPath = $obCrawler->path;

            preg_match('/\/article\/([0-9]*)\/(.*)/m', $sPath, $matches);

            $arFind['path'] = $sPath;
            $arFind['article_id'] = $matches[1];
            $arFind['code'] = $matches[2];

            $obNodeList = $finder->query("//*[contains(@class, 'primary-cat-url2')]");

            $arCategorySave = [];


            foreach ($obNodeList as $k => $item) {
                $arCategories = [
                    'code' => str_replace(['/category/', '/'], '', $item->getAttribute('href')),
                    'title' => $item->nodeValue
                ];
                $arCategorySave[] = Category::firstOrCreate($arCategories);

            }

            if($arFind['title']) {

                $obArticle = Article::updateOrCreate($arFind);


                $obArticle->categories()->saveMany($arCategorySave);

            }

        }

        /** @var \GuzzleHttp\Client $client Guzzle. */
        $client = new Client([
            'allow_redirects'=>false,
        ]);




        /** @var bool $bResume Do while not found new links */
        $bResume = true;
        /** @var int $iCount Count of crawlers */
        $iCount = DB::select("SELECT COUNT(*) AS count_crawlers FROM crawlers")[0]->count_crawlers;
        /** @var int $iStartFrom Page from start crawled */
        $iStartFrom = 0;

        while($bResume){

            //# initiate crawler
            $obCrawler = Crawler::create([RequestOptions::ALLOW_REDIRECTS => true, RequestOptions::TIMEOUT => 60]);
            $obCrawler
                ->acceptNofollowLinks()
                ->ignoreRobots()
                ->setParseableMimeTypes(['text/html', 'text/plain'])
                ->setCrawlObserver(new ArticleCrawlerObserver())
                ->setCrawlProfile(new \Spatie\Crawler\CrawlProfiles\CrawlInternalUrls($link))
                ->setMaximumResponseSize(1024 * 1024 * 2) // 2 MB maximum
                ->setTotalCrawlLimit(100 + $iStartFrom) // limit defines the maximal count of URLs to crawl
                // ->setConcurrency(1) // all urls will be crawled one by one
                ->setDelayBetweenRequests(100)
                ->startCrawling($link);

            $link = 'https://www.infoworld.com/category/web-development/?start=' . ($iStartFrom += 20);

            $iNewCount = DB::select("SELECT COUNT(*) AS count_crawlers FROM crawlers")[0]->count_crawlers;

//            Log::debug($link);

            $bResume = intval($client->request('GET',  $link)->getStatusCode()) != 301 && $iCount == $iNewCount;
        }





        return ['status'=> 200];

    }
}
