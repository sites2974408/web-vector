<?php

namespace App\Observers;

use Illuminate\Support\Facades\Cache;

class PortfolioObserver
{
    public function created()
    {
        Cache::purge('portfolio');
    }
}
