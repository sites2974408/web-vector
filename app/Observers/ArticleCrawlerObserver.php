<?php

namespace App\Observers;


use App\Models\Crawler as CrawlerModel;
use Psr\Http\Message\UriInterface;
use Illuminate\Support\Facades\Log;
use GuzzleHttp\RequestOptions;
use Psr\Http\Message\ResponseInterface;
use GuzzleHttp\Exception\RequestException;
use Spatie\Crawler\CrawlObservers\CrawlObserver;
use Spatie\Crawler\Crawler;


class ArticleCrawlerObserver extends CrawlObserver
{

    /**
     * @var string HTML content of page.
     */
    private $content = '';

    private $path = '';

    /**
     * Called when the crawler has crawled the given url successfully.
     *
     * @param \Psr\Http\Message\UriInterface $url
     * @param \Psr\Http\Message\ResponseInterface $response
     * @param \Psr\Http\Message\UriInterface|null $foundOnUrl
     */
    public function crawled(
        UriInterface $url,
        ResponseInterface $response,
        ?UriInterface $foundOnUrl = null
    ): void {

        $this->content = '';
        $this->path = '';


        if(str_contains($url, 'article')) {

            $doc = new \DOMDocument();
            @$doc->loadHTML($response->getBody());
            $finder = new \DomXPath($doc);
            $find = $finder->query("//*[contains(@class, 'blog')]")->item(0);
            if(empty($find)){
                $find = $finder->query("//*[contains(@class, 'infoworld')]")->item(0);

            }
            if($find) {
                $doc = new \DOMDocument;
                $doc->appendChild($doc->importNode($find, 1));

                /** @var \App\Observers\finder $find Search link to next page */

                $findNext = $finder->query("//*[contains(@class, 'page-link next')]")->item(0);



                $content = $doc->saveHTML();
                //# strip all javascript
                $content = preg_replace('/<script\b[^>]*>(.*?)<\/script>/is', '', $content);
                //# strip all style
                $content = preg_replace('/<style\b[^>]*>(.*?)<\/style>/is', '', $content);
                $content = str_replace(array('\t', '\n\n'), '', $content);

                $content = preg_replace('/\t/', '', $content);
                $content = preg_replace('/\n\n+/', '\n', $content);
//            $content = preg_replace('/\\\\/', '\\', $content);
//            $content = str_replace('\\\\n', '\n', $content);

                $content = str_replace(array('https://www.infoworld.com', '.html'), '', $content);


                $content = stripcslashes($content);
                $this->path = str_replace(array('https://www.infoworld.com', '.html'), '', $url);

                //# append
                $this->content = $content;
            }
        } else {
            $this->content = '';
            $this->path = '';
        }


            // Create records
            CrawlerModel::updateOrCreate([
                'url' => $url,
                'path' => $this->path,
                'content' => $this->content,
            ], [
                'status' => $response->getStatusCode()
            ]);



        if(!empty($findNext)){

            $link = 'https://www.infoworld.com' . $findNext->getAttribute('href');
            $obCrawler = Crawler::create([RequestOptions::ALLOW_REDIRECTS => true, RequestOptions::TIMEOUT => 30]);
            $obCrawler
                ->setParseableMimeTypes(['text/html', 'text/plain'])
                ->setCrawlObserver(new ArticleCrawlerObserver())
                ->setCrawlProfile(new \Spatie\Crawler\CrawlProfiles\CrawlInternalUrls($link))
                ->setMaximumResponseSize(1024 * 1024 * 2) // 2 MB maximum
                ->setTotalCrawlLimit(1) // limit defines the maximal count of URLs to crawl
                ->setDelayBetweenRequests(100)
                ->startCrawling($link);
        }


    }

    /**
     * Called when the crawler had a problem crawling the given url.
     *
     * @param \Psr\Http\Message\UriInterface $url
     * @param \GuzzleHttp\Exception\RequestException $requestException
     * @param \Psr\Http\Message\UriInterface|null $foundOnUrl
     */
    public function crawlFailed(
        UriInterface $url,
        RequestException $requestException,
        ?UriInterface $foundOnUrl = null
    ): void {
        Log::error('crawlFailed: ' . $url);
    }

    /**
     * Called when the crawl has ended.
     */
    public function finishedCrawling(): void
    {
        //
    }

}
