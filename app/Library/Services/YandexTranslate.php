<?php
/**
 * Created by PhpStorm.
 * User: deniszakusilo
 * Date: 08.02.2023
 * Time: 19:31
 */

namespace App\Library\Services;



use Jose\Component\Core\AlgorithmManager;;
use Jose\Component\Signature\Algorithm\PS256;
use Jose\Component\Core\Util\JsonConverter;
use Jose\Component\KeyManagement\JWKFactory;
use Jose\Component\Signature\JWSBuilder;
use Jose\Component\Signature\Serializer\CompactSerializer;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Storage;


class YandexTranslate extends BigTextTranslate
{
    const URL = 'https://translate.api.cloud.yandex.net/translate/v2/translate';
    protected static  $token = '';
    protected static $folder = '';

    public static function translate(Array $arText, $sLang = 'ru', $sFormat = 'HTML')
    {

        self::$symbolLimit = 5000;


        self::$token = Cache::remember('ya_token', 60*60*12, function (){
            return self::getToken();
        });

        self::$folder = env('YANDEX_FOLDER');
        $headers = [
            'Content-Type: application/json',
            "Authorization: Bearer " . self::$token
        ];



        $curl = curl_init();
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
// curl_setopt($curl, CURLOPT_VERBOSE, 1);

        curl_setopt($curl, CURLOPT_URL, self::URL);
        curl_setopt($curl, CURLOPT_POST, true);

        $post_data = [
            "targetLanguageCode" => $sLang,
            "texts" => [],
            "folderId" => self::$folder,
            "format" => $sFormat,
        ];

        /**
         * @var array $arResult Array of returned translated texts.
         */
        $arResult = [];

        /** Translate all texts in array */
        foreach ($arText as $item) {
            /**
             * @var string $sBigText returned translated text.
             */
            $sBigText = '';

            /** All text delete by string limit (YA api can translate only 10000 symbols) */
            foreach (self::toBigPieces($item) as $arBigPiece) {

                $post_data['texts'] = $arBigPiece;

                $data_json = json_encode($post_data);
                curl_setopt($curl, CURLOPT_POSTFIELDS, $data_json);

                $arCurlResult = json_decode(curl_exec($curl), true);

                if(array_key_exists('translations', $arCurlResult)) {
                    $sBigText .= $arCurlResult['translations'][0]['text'];
                }
            }
            array_push($arResult, $sBigText);
        }

        curl_close($curl);

        return $arResult;

    }

    private static function getToken(){
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'https://iam.api.cloud.yandex.net/iam/v1/tokens');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, "{\"yandexPassportOauthToken\":\"" . env('YANDEX_ACCOUNT_ID') . "\"}");

        $headers = array();
        $headers[] = 'Content-Type: application/x-www-form-urlencoded';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = json_decode(curl_exec($ch));
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }
        curl_close($ch);

        return $result->iamToken;
    }

    /**
     * @return string Token
     */
    private static function getServiceToken(){
        $service_account_id = env('YANDEX_SERVICE_ID');
        $key_id = Storage::path('/keys/yandex/private_key');

        $jsonConverter = new JsonConverter();
        $algorithmManager = new AlgorithmManager([
            new PS256(),
        ]);

        $jwsBuilder = new JWSBuilder($algorithmManager);

        $now = time();

        $claims = [
            'aud' => 'https://iam.api.cloud.yandex.net/iam/v1/tokens',
            'iss' => $service_account_id,
            'iat' => $now,
            'exp' => $now + 360
        ];

        $header = [
            'alg' => 'PS256',
            'typ' => 'JWT',
            'kid' => $key_id
        ];

        $key = JWKFactory::createFromKeyFile($key_id);

        $payload = $jsonConverter->encode($claims);

// Формирование подписи.
        $jws = $jwsBuilder
            ->create()
            ->withPayload($payload)
            ->addSignature($key, $header)
            ->build();

        $serializer = new CompactSerializer($jsonConverter);

// Формирование JWT.
        return $serializer->serialize($jws);
    }
}
