<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\app\Models\Traits\CrudTrait as CrudTrait;

class Crawler extends Model
{
    use HasFactory, CrudTrait;
    /**
     * @var string $table Таблица БД, ассоциированная с моделью.
     */
    protected $table = 'crawlers';

    /**
     * @var string $primaryKey Первичный ключ таблицы БД.
     */
    protected $primaryKey = 'id';

    protected $fillable = ['url', 'status', 'path', 'content'];
}
