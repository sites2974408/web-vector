<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\app\Models\Traits\CrudTrait as CrudTrait;

class Article extends Model
{
    use HasFactory, CrudTrait;
    /**
     * @var string $table Таблица БД, ассоциированная с моделью.
     */
    protected $table = 'articles';

    /**
     * @var string $primaryKey Первичный ключ таблицы БД.
     */
    protected $primaryKey = 'id';

    protected $fillable = ['article_id', 'code', 'title', 'img', 'body', 'description', 'path'];

    public function categories()
    {
        //return $this->belongsToMany(RelatedModel, pivot_table_name, foreign_key_of_current_model_in_pivot_table, foreign_key_of_other_model_in_pivot_table);
        return $this->belongsToMany(
            Category::class,
            'categories_articles',
            'article_id',
            'category_id');
    }


}
