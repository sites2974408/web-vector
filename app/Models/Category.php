<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Backpack\CRUD\app\Models\Traits\CrudTrait as CrudTrait;

class Category extends Model
{
    use HasFactory, CrudTrait;
    /**
     * @var string $table Таблица БД, ассоциированная с моделью.
     */
    protected $table = 'categories';

    /**
     * @var string $primaryKey Первичный ключ таблицы БД.
     */
    protected $primaryKey = 'id';

    protected $fillable = ['code', 'title'];

    public function articles()
    {
        //return $this->belongsToMany(RelatedModel, pivot_table_name, foreign_key_of_current_model_in_pivot_table, foreign_key_of_other_model_in_pivot_table);
        return $this->belongsToMany(
            Article::class,
            'categories_articles',
            'article_id',
            'category_id');
    }
}
