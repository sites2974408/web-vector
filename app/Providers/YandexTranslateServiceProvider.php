<?php

namespace App\Providers;

use App\Library\Services\YandexTranslate;
use Illuminate\Support\ServiceProvider;

class YandexTranslateServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('App\Library\Services\YandexTranslate', function ($app) {
            return new YandexTranslate();
        });
        $this->app->bind('App\Library\Services\BigTextTranslate', function ($app) {
            return new BigTextTranslate();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
