<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="generator" content="Hugo 0.84.0">

    <title>Создание и разработка сайтов под ключ</title>
    <meta name="keywords" content="создание сайтов, сайт под ключ, разработка веб сайтов, bitrix, bitrix ru, битрикс, сайт на битрикс, битрикс товары, разработка сайтов, сделать сайт, laravel, vue">
    <meta name="description" content="Разработка и создание сайтов любой сложности по низким ценам">
    <meta property="og:image"
          content="https://web-vector.ru/images/index/top_image.png">
    <meta property="og:image:width" content="1280">
    <meta property="og:image:height" content="670">
    <meta name="twitter:image"
          content="https://web-vector.ru/images/index/top_image.png">
    <meta name="vk:image"
          content="https://web-vector.ru/images/index/top_image.png">
    <meta property="og:image" content="https://web-vector.ru/images/index/top_image.png">
    <meta property="og:image:width" content="400">
    <meta property="og:image:height" content="400">
    <meta property="og:title"
          content="Разработка и создание сайтов любой сложности по низким ценам">
    <meta property="og:description"
          content="Разработка и создание сайтов любой сложности по низким ценам">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" type="image/x-icon" href="/images/index/favicon.ico">
    <!-- Yandex.Metrika counter -->
    <script type="text/javascript" >
        (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
            m[i].l=1*new Date();
            for (var j = 0; j < document.scripts.length; j++) {if (document.scripts[j].src === r) { return; }}
            k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
        (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

        ym(91345060, "init", {
            clickmap:true,
            trackLinks:true,
            accurateTrackBounce:true,
            webvisor:true
        });
    </script>
    <noscript><div><img src="https://mc.yandex.ru/watch/91345060" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
    <!-- /Yandex.Metrika counter -->


    @vite('resources/css/V1/app.scss')
</head>
<body>

    <div id="app"></div>

<footer class="container py-5">
    <div class="row">
        <div class="col-12 col-md">
            <small class="d-block mb-3 text-muted">&copy; 2022–2023</small>
        </div>
        <div class="col-6 col-md">
            <ul class="list-unstyled text-small">
                <li><a class="link-secondary" href="/portfolio">Портфолио</a></li>
            </ul>
        </div>
        <div class="col-6 col-md">
            <ul class="list-unstyled text-small">
                <li><a class="link-secondary" href="/price">Цены</a></li>
            </ul>
        </div>
        <div class="col-6 col-md">
            <ul class="list-unstyled text-small">
                <li><a class="link-secondary" href="/article">Новости ИТ</a></li>
            </ul>
        </div>
        <div class="col-6 col-md">
            <ul class="list-unstyled text-small">
                <li><a class="link-secondary" href="/contact">Контакты</a></li>
            </ul>
        </div>
    </div>
    @vite('resources/js/app.js')
</footer>

</body>

</html>
