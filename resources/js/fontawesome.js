/* import the fontawesome core */
import { library } from '@fortawesome/fontawesome-svg-core'

/* import font awesome icon component */
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

/* import specific icons */
import {
    faAddressCard,
    faHome,
    faGlobe,
    faForward,
    faBackward,
    faComment,
    faNewspaper,
    faMobileScreen,
    faShop,
    faRubleSign,
    faArrowCircleLeft,
    faArrowCircleRight,
    faArrowAltCircleLeft,
    faArrowAltCircleRight,
} from '@fortawesome/free-solid-svg-icons'


/* add icons to the library */
library.add(
    faAddressCard,
    faHome,
    faGlobe,
    faForward,
    faBackward,
    faComment,
    faNewspaper,
    faMobileScreen,
    faShop,
    faRubleSign,
    faArrowCircleLeft,
    faArrowCircleRight,
    faArrowAltCircleLeft,
    faArrowAltCircleRight
)

export default FontAwesomeIcon;
