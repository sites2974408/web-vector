import './bootstrap';
import { createApp } from 'vue';

const sVersion = import.meta.env.VITE_APP_JS_VERSION;

import {router, components as includedComponents} from './router';
import App from './App.vue';

import FontAwesomeIcon from './fontawesome';
import { Carousel, Slide, Pagination, Navigation } from 'vue3-carousel'



const app = createApp(App);
app.use(router);
// app.use(FlashMessage);





Object.entries(includedComponents).forEach(([path, definition]) => {
    // Get name of component, based on filename
    // "./components/Fruits.vue" will become "Fruits"
    const componentName = path.split('/').pop().replace(/\.\w+$/, '')

    // Register component on this Vue instance
    app.component(componentName, definition.default)
})

app
    .component('Carousel', Carousel)
    .component('Slide', Slide)
    .component('Pagination', Pagination)
    .component('Navigation', Navigation)
    .component('FontAwesomeIcon', FontAwesomeIcon)
    .mount('#app');
