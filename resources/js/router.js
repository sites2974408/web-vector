import {routes as r1, components as c1} from './router_v1';
import {routes as r2, components as c2} from './router_v2';



import {createRouter, createWebHistory} from "vue-router/dist/vue-router";


const sVersion = import.meta.env.VITE_APP_JS_VERSION;

let  routes = [];
let exportComponents = {};

switch (sVersion) {
    case 'V1':
        routes = r1;
        exportComponents = c1;
        break;

    case 'V2':
        routes = r2;
        exportComponents = c2;
        break;
}

localStorage.setItem('routes', JSON.stringify(routes));
localStorage.setItem('visited', JSON.stringify([]));


const router = createRouter({
    history: createWebHistory(),
    mode: 'history',
    routes,
});
export {router, exportComponents as components}
