import { ref } from 'vue'
import axios from 'axios'
import { useRouter } from 'vue-router'

export default function useArticles() {
    const article = ref([])
    const articles = ref([])

    const errors = ref('')
    const router = useRouter()

    const getArticles = () => {
        let response = axios.get('/api/v1/article')
            .then(function (response) {
                articles.value = response.data.data
        })
        articles.value = response.data.data
    }

    const getArticle = async (id) => {
        let response = await axios.get(`/api/v1/article/${id}`)
        article.value = response.data.data
    }

    const storeArticle = async (data) => {
        errors.value = ''
        try {
            await axios.post('/api/v1/article', data)
            await router.push({ name: 'articles.index' })
        } catch (e) {
            if (e.response.status === 422) {
                for (const key in e.response.data.errors) {
                    errors.value = e.response.data.errors
                }
            }
        }

    }

    const updateArticle = async (id) => {
        errors.value = ''
        try {
            await axios.patch(`/api/v1/articles/${id}`, article.value)
            await router.push({ name: 'articles.index' })
        } catch (e) {
            if (e.response.status === 422) {
                for (const key in e.response.data.errors) {
                    errors.value = e.response.data.errors
                }
            }
        }
    }

    return {
        errors,
        article,
        articles,
        getArticle,
        getArticles,
        storeArticle,
        updateArticle
    }
}
