import { createWebHistory, createRouter } from 'vue-router';

import Home from './V1/pages/Home.vue';
import Article from './V1/pages/Article.vue';
import ArticleDetail from './V1/pages/ArticleDetail.vue';
import Contact from './V1/pages/Contact.vue';
import Portfolio from './V1/pages/Portfolio.vue'
import Price from './V1/pages/Price.vue'

const routes = [
    { path: '/', name: 'main', title: 'Главная', component: Home , icon: 'fas fa-home'},
    { path: '/contact', name: 'contacts', title: 'Контакты', component: Contact , icon: "fas fa-address-card"},

    { path: '/article/', name: 'articles', title: 'Новости ИТ', component: Article , icon: "fas fa-newspaper"},
    { path: '/article/:id/:code(.*)', name: 'article.detail', title: 'Статья', component: ArticleDetail , icon: "fas fa-newspaper"},

    { path: '/portfolio/', name: 'portfolios', title: 'Наши работы', component: Portfolio , icon: "fas fa-globe"},
    { path: '/price/', name: 'pricies', title: 'Цены на работу', component: Price , icon: "fas fa-ruble-sign"},
];



const pages = import.meta.globEager('./V2/pages/*.vue');

const components = import.meta.globEager('./V2/components/*.vue');
const blocks = import.meta.globEager('./V2/components/blocks/*.vue');

let exportComponents = {...components, ...pages, ...blocks};

export {routes , exportComponents as components};
