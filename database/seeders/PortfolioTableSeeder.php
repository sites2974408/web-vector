<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\File;
use function League\Flysystem\path;

class PortfolioTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $table = \DB::table('portfolios');

        $table->truncate();

        $sStoragePath = 'images/portfolio';
        $sFilePath = config('filesystems.disks')['root']['root'] . '/public/index/portfolio/';

        Storage::deleteDirectory($sStoragePath);

        $table->insert([
                'title' => '9 Жизней',
                'image' => Storage::putFile(
                    $sStoragePath,
                    new File(
                        $sFilePath . '9zhizney1.jpg'
                    )
                ),
                'description' => 'Ветеренарная клиника',
                'path' => 'http://9zhizney.ru/',
                'type' => 'web',
                'technology' => 'Bitrix'
            ]
        );

        $table->insert(
            [
                'title' => 'Технониколь',
                'image' => Storage::putFile(
                    $sStoragePath,
                    new File(
                        $sFilePath . 'teplo-tn-lp1.jpg'
                    )
                ),
                'description' => 'Калькулятор выбора материала ("ТехноНИКОЛЬ")',
                'path' => 'https://teplo.tn.ru/calcs/ee/',
                'type' => 'web',
                'technology' => 'Bitrix, React.js'
            ]
        );

        $table->insert(
            [
                'title' => 'Утренняя звезда',
                'description' => 'Сервис доставки воды "Утренняя Звезда"',
                'path' => 'https://v-oda.ru/',
                'type' => 'shop',
                'technology' => 'Bitrix',
                'image' => Storage::putFile(
                    $sStoragePath,
                    new File(
                        $sFilePath . 'v-oda1.jpg'
                    )
                ),
            ]
        );


        $table->insert(
            [
                'title' => 'Datsun',
                'description' => 'Официальный дилер Datsun',
                'path' => '',
                'type' => 'web',
                'technology' => 'Bitrix',
                'image' => Storage::putFile(
                    $sStoragePath,
                    new File(
                        $sFilePath . 'megaalians-datsun1.jpg'
                    )
                ),
            ]
        );

        $table->insert(
            [
                'title' => 'LendsBay',
                'description' => 'Онлайн-займы на своих условиях',
                'path' => 'https://lendsbay.com/',
                'type' => 'app',
                'technology' => 'React-native, python, iohttp',
                'image' => Storage::putFile(
                    $sStoragePath,
                    new File(
                        $sFilePath . 'phoneone.png'
                    )
                ),
            ]
        );

        $table->insert(
            [
                'title' => 'Гута клиника',
                'description' => 'ГУТА КЛИНИК, медицинский центр',
                'path' => 'https://gutaclinic.ru/',
                'type' => 'web',
                'technology' => 'Bitrix',
                'image' => Storage::putFile(
                    $sStoragePath,
                    new File(
                        $sFilePath . 'clinic.JPG'
                    )
                ),
            ]
        );


        $table->insert(
            [
                'title' => 'Uniconf',
                'description' => 'Объединённые кондитеры — кондитерский холдинг',
                'path' => 'https://www.uniconf.ru/',
                'type' => 'web',
                'technology' => 'Bitrix',
                'image' => Storage::putFile(
                    $sStoragePath,
                    new File(
                        $sFilePath . 'uniconf.JPG'
                    )
                ),
            ]
        );


        $table->insert(
            [
                'title' => 'Savoy',
                'description' => 'Пятизвездочный отель "Савой", г. Москва',
                'path' => 'https://savoy.ru/',
                'type' => 'web',
                'technology' => 'Bitrix',
                'image' => Storage::putFile(
                    $sStoragePath,
                    new File(
                        $sFilePath . 'savoy.jpg'
                    )
                ),
            ]
        );


        $table->insert(
            [
                'title' => 'Coral strand',
                'description' => 'Отель Coral Strand 4* на Сейшелах',
                'path' => 'https://coralstrand.com/ru/',
                'type' => 'web',
                'technology' => 'Bitrix',
                'image' => Storage::putFile(
                    $sStoragePath,
                    new File(
                        $sFilePath . 'coralstrand.jpg'
                    )
                ),
            ]
        );


        $table->insert(
            [
                'title' => 'Перекресток',
                'description' => 'Vprok.ru Перекрёсток – доставка продуктов на дом',
                'path' => 'https://www.vprok.ru/',
                'type' => 'shop',
                'technology' => 'React, Laravel, MicroServices',
                'image' => Storage::putFile(
                    $sStoragePath,
                    new File(
                        $sFilePath . 'vprok.JPG'
                    )
                ),
            ]
        );


        $table->insert(
            [
                'title' => 'Eldorado',
                'description' => 'Эльдорадо - интернет-магазин электроники',
                'path' => 'https://www.eldorado.ru/',
                'type' => 'shop',
                'technology' => 'Bitrix, React.js, MicroServices',
                'image' => Storage::putFile(
                    $sStoragePath,
                    new File(
                        $sFilePath . 'eldorado.JPG'
                    )
                ),
            ]
        );


        $table->insert(
            [
                'title' => 'Alenka',
                'description' => '«Алёнка» — интернет-магазин',
                'path' => 'https://www.alenka.ru/',
                'type' => 'shop',
                'technology' => 'Bitrix, Vue.js',
                'image' => Storage::putFile(
                    $sStoragePath,
                    new File(
                        $sFilePath . 'alenka.JPG'
                    )
                ),
            ]
        );
    }
}
